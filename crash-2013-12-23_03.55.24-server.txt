---- Minecraft Crash Report ----
// Shall we play a game?

Time: 12/23/13 3:55 AM
Description: Exception in server tick loop

cpw.mods.fml.common.MissingModsException
	at cpw.mods.fml.common.Loader.sortModList(Loader.java:244)
	at cpw.mods.fml.common.Loader.loadMods(Loader.java:491)
	at cpw.mods.fml.server.FMLServerHandler.beginServerLoading(FMLServerHandler.java:99)
	at cpw.mods.fml.common.FMLCommonHandler.onServerStart(FMLCommonHandler.java:350)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:92)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:634)
	at net.minecraft.server.ThreadMinecraftServer.run(ThreadMinecraftServer.java:16)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.6.4
	Operating System: Linux (amd64) version 3.2.0-4-amd64
	Java Version: 1.7.0_45, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 714460088 bytes (681 MB) / 1038876672 bytes (990 MB) up to 4260102144 bytes (4062 MB)
	JVM Flags: 9 total; -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=4 -XX:+AggressiveOpts -XX:MaxPermSize=512m -XX:PermSize=512m -Xms1G -Xmx4G
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Suspicious classes: FML and Forge are installed
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	CraftBukkit Information: 
   Running: 
   Failed to handle CraftCrashReport:
java.lang.NullPointerException
	at org.bukkit.Bukkit.getName(Bukkit.java:72)
	at org.bukkit.craftbukkit.v1_6_R3.CraftCrashReport.call(CraftCrashReport.java:19)
	at net.minecraft.crash.CrashReportCategory.func_71500_a(CrashReportCategory.java:106)
	at net.minecraft.crash.CrashReport.func_71504_g(CrashReport.java:58)
	at net.minecraft.crash.CrashReport.<init>(CrashReport.java:40)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:693)
	at net.minecraft.server.ThreadMinecraftServer.run(ThreadMinecraftServer.java:16)

	FML: MCP v8.11 FML v6.4.49.965 Minecraft Forge 9.11.1.965 130 mods loaded, 130 mods active
	mcp{8.09} [Minecraft Coder Pack] (minecraft.jar) Unloaded
	FML{6.4.49.965} [Forge Mod Loader] (mcpc.b186.jar) Unloaded
	Forge{9.11.1.965} [Minecraft Forge] (mcpc.b186.jar) Unloaded
	CodeChickenCore{0.9.0.6} [CodeChicken Core] (minecraft.jar) Unloaded
	Evoc{1.0.0} [Evoc] (minecraft.jar) Unloaded
	InfiniBows{1.2.0 build 14} [Infinity Bow Fix] (minecraft.jar) Unloaded
	MobiusCore{1.0.4} [MobiusCore] (minecraft.jar) Unloaded
	NotEnoughItems{1.6.1.5} [Not Enough Items] (NotEnoughItems 1.6.1.5.jar) Unloaded
	PowerCrystalsCore{1.1.8} [PowerCrystals Core] (PowerCrystalsCore-1.1.8-9.jar) Unloaded
	TConstruct-Preloader{0.0.1} [Tinkers Corestruct] (minecraft.jar) Unloaded
	AppliedEnergistics-Core{rv14.finale2} [AppliedEnergistics Core] (minecraft.jar) Unloaded
	denLib{3.1.35} [denLib] (minecraft.jar) Unloaded
	switches|pistontweak{1.3.0 build 25} [Switches|PistonTweak] (minecraft.jar) Unloaded
	arsmagica2{1.0.2b} [Ars Magica 2] (AM2_1.0.2b.zip) Unloaded
	AppliedEnergistics{rv14.finale2} [Applied Energistics] (appeng-rv14-finale2-mc16x.jar) Unloaded
	BiblioCraft{1.5.3} [BiblioCraft] (BiblioCraft[v1.5.3].zip) Unloaded
	BiblioWoodsBoP{1.3} [BiblioWoods Biomes O'Plenty Edition] (BiblioWoods[BiomesOPlenty][v1.3].zip) Unloaded
	BiblioWoodsForestry{1.3} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.3].zip) Unloaded
	BiblioWoodsNatura{1.1} [BiblioWoods Natura Edition] (BiblioWoods[Natura][v1.1].zip) Unloaded
	Billund{1.01} [BILLUND] (BILLUND1.01.zip) Unloaded
	BiomesOPlenty{1.1.3} [Biomes O' Plenty] (BiomesOPlenty-universal-1.6.4-1.1.3.308.jar) Unloaded
	BuildCraft|Builders{4.1.2} [BC Builders] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	BuildCraft|Core{4.1.2} [BuildCraft] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	BuildCraft|Energy{4.1.2} [BC Energy] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	BuildCraft|Factory{4.1.2} [BC Factory] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	BuildCraft|Silicon{4.1.2} [BC Silicon] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	BuildCraft|Transport{4.1.2} [BC Transport] (buildcraft-A-1.6.2-4.1.2.jar) Unloaded
	Additional-Buildcraft-Objects{1.0.6.182} [Additional Buildcraft Objects] (buildcraft-Z-additional-buildcraft-objects-1.0.6.182.jar) Unloaded
	ChickenChunks{1.3.3.3} [ChickenChunks] (ChickenChunks 1.3.3.3.jar) Unloaded
	CoFHCore{2.0.0.b9e} [CoFH Core] (CoFHCore-2.0.0.b9e.jar) Unloaded
	CoFHLoot{2.0.0.b9e} [CoFH Loot] (CoFHCore-2.0.0.b9e.jar) Unloaded
	CoFHMasquerade{2.0.0.b9e} [CoFH Masquerade] (CoFHCore-2.0.0.b9e.jar) Unloaded
	CoFHSocial{2.0.0.b9e} [CoFH Social] (CoFHCore-2.0.0.b9e.jar) Unloaded
	CoFHWorld{2.0.0.b9e} [CoFH World] (CoFHCore-2.0.0.b9e.jar) Unloaded
	CompactSolars{4.4.19.224} [Compact Solar Arrays] (compactsolars-universal-1.6.4-4.4.19.224.zip) Unloaded
	CCTurtle{1.57} [ComputerCraft Turtles] (ComputerCraft1.57.zip) Unloaded
	ComputerCraft{1.57} [ComputerCraft] (ComputerCraft1.57.zip) Unloaded
	DenPipes{2.1.18} [DenPipes] (DenPipes-1.6.4-2.1.18.jar) Unloaded
	DenPipes-Emerald{1.1.6} [DenPipes-Emerald] (DenPipes-Emerald-1.6.4-1.1.6.jar) Unloaded
	DenPipes-Forestry{1.1.8} [DenPipes-Forestry] (DenPipes-Forestry-1.6.4-1.1.8.jar) Unloaded
	EnderStorage{1.4.3.5} [EnderStorage] (EnderStorage 1.4.3.5.jar) Unloaded
	ExtraUtilities{1.0.1} [Extra Utilities] (extrautils-1.0.2.zip) Unloaded
	factorization.misc{0.8.18} [Factorization Miscellaneous Nonsense] (Factorization-0.8.18.jar) Unloaded
	factorization.notify{0.8.18} [Factorization Notification System] (Factorization-0.8.18.jar) Unloaded
	factorization{0.8.18} [Factorization] (Factorization-0.8.18.jar) Unloaded
	factorization.dimensionalSlice{0.8.18} [Factorization Dimensional Slices] (Factorization-0.8.18.jar) Unloaded
	flatsigns{1.4.0} [Flat Signs] (flatsigns-1.6.2-universal-1.4.0.15.jar) Unloaded
	Forestry{2.3.1.0} [Forestry for Minecraft] (Forestry-A-2.3.1.0.jar) Unloaded
	GateCopy{3.1.4} [GateCopy] (GateCopy-1.6.4-3.1.4.jar) Unloaded
	GraviGun{2.0.0} [GraviGun] (GravityGun2.0.0.zip) Unloaded
	Hats{2.0.2} [Hats] (Hats2.0.2.zip) Unloaded
	HatStand{2.0.0} [HatStand] (HatStand2.0.0.zip) Unloaded
	IC2NuclearControl{1.6.2c} [Nuclear Control] (IC2NuclearControl-1.6.2c-ic2-experimental.zip) Unloaded
	iChunUtil{2.3.0} [iChunUtil] (iChunUtil2.3.0.zip) Unloaded
	IC2{2.0.316-experimental} [IndustrialCraft 2] (industrialcraft-2_2.0.316-experimental.jar) Unloaded
	inventorytweaks{1.56} [Inventory Tweaks] (InventoryTweaks-MC1.6.2-1.56-b77.jar) Unloaded
	IronChest{5.4.1.649} [Iron Chest] (ironchest-universal-1.6.4-5.4.1.649.zip) Unloaded
	LogisticsPipes|Main{0.7.4.dev.80} [Logistics Pipes] (LogisticsPipes-MC1.6.4-0.7.4.dev.80.jar) Unloaded
	MagicBees{2.1.7} [Magic Bees] (magicbees-2.1.7.jar) Unloaded
	MFFS{3.5.0} [Modular Force Field System] (MFFS_v3.5.0.255.jar) Unloaded
	MFR Compat Forestry Trees{1.0} [MFR Compat Forestry Trees] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MFR Compat Extra Trees{1.0} [MFR Compat Extra Trees] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded{1.6.2R2.7.4B1} [MineFactory Reloaded] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatAppliedEnergistics{1.6.2R2.7.4B1} [MFR Compat: Applied Energistics] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatAtum{1.6.2R2.7.4B1} [MFR Compat: Atum] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatBackTools{1.6.2R2.7.4B1} [MFR Compat: BackTools] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatBuildCraft{1.6.2R2.7.4B1} [MFR Compat: BuildCraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatChococraft{1.6.2R2.7.4B1} [MFR Compat: Chococraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatExtraBiomes{1.6.2R2.7.4B1} [MFR Compat: ExtraBiomes] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatForestry{1.6.2R2.7.4B1} [MFR Compat: Forestry] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatForestryPre{1.6.2R2.7.4B1} [MFR Compat: Forestry (part 2)] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatForgeMicroblock{1.6.2R2.7.4B1} [MFR Compat: ForgeMicroblock] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatIC2{1.6.2R2.7.4B1} [MFR Compat: IC2] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatMagicalCrops{1.6.2R2.7.4B1} [MFR Compat: Magical Crops] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatMystcraft{1.6.2R2.7.4B1} [MFR Compat: Mystcraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatPams{1.6.2R2.7.4B1} [MFR Compat: Pam's Mods] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatRailcraft{1.6.2R2.7.4B1} [MFR Compat: Railcraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatRP2{1.6.2R2.7.4B1} [MFR Compat: RP2] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatSufficientBiomes{1.6.2R2.7.4B1} [MFR Compat: Sufficient Biomes] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatThaumcraft{1.6.2R2.7.4B1} [MFR Compat: Thaumcraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatThermalExpansion{1.6.2R2.7.4B1} [MFR Compat: Thermal Expansion] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatTwilightForest{1.6.2R2.7.4B1} [MFR Compat: TwilightForest] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatVanilla{1.6.2R2.7.4B1} [MFR Compat: Vanilla] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	MineFactoryReloaded|CompatXyCraft{1.6.2R2.7.4B1} [MFR Compat: XyCraft] (MineFactoryReloaded-2.7.4B1-215.jar) Unloaded
	powersuits{0.9.0-77} [MachineMuse's Modular Powersuits] (ModularPowersuits-1.6.2-0.9.0-77.jar) Unloaded
	Morph{0.4.0} [Morph] (Morph-Beta-0.4.0.zip) Unloaded
	Morpheus{1.2} [Morpheus] (Morpheus-1.6.4-1.2.32.jar) Unloaded
	Mystcraft{0.10.11.00} [Mystcraft] (mystcraft-uni-1.6.4-0.10.11.00.zip) Unloaded
	Natura{2.1.12} [Natura] (Natura_1.6.4_2.1.13.jar) Unloaded
	NEIAddons{1.9.3.r47} [NEI Addons] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIAddons|AE{1.9.3.r47} [NEI Addons: Applied Energistics] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIAddons|CraftingTables{1.9.3.r47} [NEI Addons: Crafting Tables] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIAddons|ExtraBees{1.9.3.r47} [NEI Addons: Extra Bees] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIAddons|Forestry{1.9.3.r47} [NEI Addons: Forestry] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIAddons|MiscPeripherals{1.9.3.r47} [NEI Addons: Misc Peripherals] (neiaddons-1.6.2-1.9.3.r47.jar) Unloaded
	NEIPlugins{1.1.0.6} [NEI Plugins] (NEIPlugins-1.1.0.6.jar) Unloaded
	numina{0.1.0-46} [Numina] (Numina-1.6.2-0.1.0-46.jar) Unloaded
	ObsidiPlates{2.0.0} [Obsidian Pressure Plates] (obsidiplates-1.6.2-universal-2.0.0.15.jar) Unloaded
	OpenBlocks{1.2.2} [OpenBlocks] (OpenBlocks-1.2.2.jar) Unloaded
	OpenPeripheral{0.2.1} [OpenPeripheral] (OpenPeripheral-0.2.1-preview8.jar) Unloaded
	MapWriter{2.0.1} [MapWriter] (Opis_1.1.0_alpha.zip) Unloaded
	Opis{1.1.0_alpha} [Opis] (Opis_1.1.0_alpha.zip) Unloaded
	PluginsforForestry{3.2.30} [PluginsforForestry] (PluginsforForestry-1.6.4-3.2.30.jar) Unloaded
	PortalGun{2.0.1} [PortalGun] (PortalGun2.0.1.zip) Unloaded
	ProjRed|Core{4.1.0.13} [Project: Red-Core] (ProjectRedBase-1.6.4-4.1.0.13.jar) Unloaded
	ProjRed|Compatability{4.1.0.13} [ProjectRed-Compatability] (ProjectRedCompat-1.6.4-4.1.0.13.jar) Unloaded
	ProjRed|Integration{4.1.0.13} [ProjectRed-Integration] (ProjectRedIntegration-1.6.4-4.1.0.13.jar) Unloaded
	ProjRed|Transmission{4.1.0.13} [ProjectRed-Transmission] (ProjectRedIntegration-1.6.4-4.1.0.13.jar) Unloaded
	ProjRed|Illumination{4.1.0.13} [ProjectRed-Illumination] (ProjectRedLighting-1.6.4-4.1.0.13.jar) Unloaded
	ProjRed|Exploration{4.1.0.13} [ProjectRed-Exploration] (ProjectRedWorld-1.6.4-4.1.0.13.jar) Unloaded
	QuantumCraft{1.02_mc164} [qCraft] (qCraft1.02_mc164.zip) Unloaded
	Railcraft{8.3.0.0} [Railcraft] (Railcraft_1.6.4-8.3.0.0.jar) Unloaded
	Redstone Arsenal{1.0.0.b0e} [Redstone Arsenal] (RedstoneArsenal-1.0.0.b0e.jar) Unloaded
	Roguelike{1.2.7} [Roguelike Dungeons Mod] (RoguelikeDungeonsForge-1.6.4-1.2.7.zip) Unloaded
	StevesCarts{2.0.0.b1} [Steve's Carts 2] (StevesCarts2.0.0.b3.zip) Unloaded
	switches{1.3.0} [Switches] (switches-1.6.4-universal-coremod-1.3.0.25.jar) Unloaded
	TConstruct{1.6.X_1.5.2.1} [Tinkers' Construct] (TConstruct_mc1.6.4_1.5.2.1.jar) Unloaded
	Thaumcraft{4.0.5b} [Thaumcraft] (Thaumcraft4.0.5b.zip) Unloaded
	ThaumicTinkerer{2.0} [Thaumic Tinkerer] (ThaumicTinkerer 2.0-44.jar) Unloaded
	ThermalExpansion{3.0.0.b9e} [Thermal Expansion] (ThermalExpansion-3.0.0.b9e.jar) Unloaded
	TMechworks{DEV.21b1027} [Tinkers' Mechworks] (TMechworks_1.6.4_0.1.2.jar) Unloaded
	Translocator{1.1.0.13} [Translocator] (Translocator 1.1.0.13.jar) Unloaded
	TwilightForest{1.20.2} [The Twilight Forest] (twilightforest-1.20.2.jar) Unloaded
	Waila{1.4.1} [Waila] (Waila_1.4.1.zip) Unloaded
	WR-CBE|Addons{1.4.0.6} [WR-CBE Addons] (WR-CBE 1.4.0.6.jar) Unloaded
	WR-CBE|Core{1.4.0.6} [WR-CBE Core] (WR-CBE 1.4.0.6.jar) Unloaded
	WR-CBE|Logic{1.4.0.6} [WR-CBE Logic] (WR-CBE 1.4.0.6.jar) Unloaded
	ForgeMicroblock{1.0.0.220} [Forge Microblocks] (ForgeMultipart-universal-1.6.4-1.0.0.220.jar) Unloaded
	ForgeMultipart{1.0.0.220} [Forge Multipart] (ForgeMultipart-universal-1.6.4-1.0.0.220.jar) Unloaded
	McMultipart{1.0.0.220} [Minecraft Multipart Plugin] (ForgeMultipart-universal-1.6.4-1.0.0.220.jar) Unloaded
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'mcpc,craftbukkit,fml,forge'
	Type: Dedicated Server (map_server.txt)